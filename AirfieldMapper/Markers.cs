﻿namespace RurouniJones.DCS.AirfieldMapper
{
    public class Markers
    {
        public Marker[] SavedMarkers { get; set; }
    }

    public class Marker
    {
        public double Lat { get; set; }
        public double Lon { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return $"{Name ?? "(unnamed)"} @ {Lat} {Lon}";
        }

        public Airfield.NavigationPoint NavigationPoint = null;

    }


}
