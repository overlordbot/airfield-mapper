﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace RurouniJones.DCS.AirfieldMapper.ChildWindows
{
    /// <summary>
    /// Interaction logic for NodeListWindow.xaml
    /// </summary>
    public partial class NodeEditorWindow : Window
    {
        private readonly MainWindow _parent;
        private readonly Airfield _airfield;
        public NodeEditorWindow(MainWindow parent, Airfield airfield)
        {
            InitializeComponent();

            _parent = parent;
            _airfield = airfield;

            RefreshButton_Click(this, new RoutedEventArgs());
        }

        private void RunwayList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0) return;
            var rwy = ((Airfield.Runway)(e.AddedItems[0]));
            RunwayNameBox.Text = rwy.Name;
            RunwayLatBox.Text = rwy.Latitude.ToString();
            RunwayLongBox.Text = rwy.Longitude.ToString();
            RunwayHeadingBox.Text = rwy.Heading.ToString();

            _parent.HighlightedPoint = rwy;
            _parent.DisplayGraph();
        }

        private void RunwaySave_Click(object sender, RoutedEventArgs e)
        {
            var rwy = ((Airfield.Runway)(RunwayList.SelectedItem));
            if (rwy == null) return;

            var oldName = rwy.Name;
            rwy.Name = RunwayNameBox.Text;
            try
            {
                rwy.Latitude = Double.Parse(RunwayLatBox.Text);
                rwy.Longitude = Double.Parse(RunwayLongBox.Text);
                rwy.Heading = Int32.Parse(RunwayHeadingBox.Text);
            }
            catch
            {
                MessageBox.Show("Failed to interpret one of your inputs to a valid number. Data not saved.");
            }
            foreach (var f in _airfield.NavigationPaths)
            {
                if (f.Source == oldName) f.Source = rwy.Name;
                if (f.Target == oldName) f.Target = rwy.Name;
            }
            _parent.DisplayGraph();
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            RunwayList.Items.Clear();
            foreach (var x in _airfield.Runways) RunwayList.Items.Add(x);
            JunctionList.Items.Clear();
            foreach (var x in _airfield.Junctions) JunctionList.Items.Add(x);
            ParkingSpotBox.Items.Clear();
            foreach (var x in _airfield.ParkingSpots) ParkingSpotBox.Items.Add(x);
        }

        private void JunctionList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0) return;
            var jct = ((Airfield.Junction)(e.AddedItems[0]));
            JunctionNameBox.Text = jct.Name;
            JunctionLatBox.Text = jct.Latitude.ToString();
            JunctionLongBox.Text = jct.Longitude.ToString();

            _parent.HighlightedPoint = jct;
            _parent.DisplayGraph();
        }

        private void RunwayDelete_Click(object sender, RoutedEventArgs e)
        {
            var rwy = ((Airfield.Runway)(RunwayList.SelectedItem));

            if (rwy == null) return;
            var edgesToRemove = new List<Airfield.NavigationPath>();

            foreach (var f in _airfield.NavigationPaths)
            {
                if (f.Source == rwy.Name) edgesToRemove.Add(f);
                else if (f.Target == rwy.Name) edgesToRemove.Add(f);
            }
            MessageBoxResult approval;
            if (edgesToRemove.Count != 0)
            {
                approval = MessageBox.Show($"Node {rwy.Name} has {edgesToRemove.Count} connections. Continuing will remove them as well. Continue to remove node?", "Node has connections", MessageBoxButton.YesNo, MessageBoxImage.Question);
            }
            else approval = MessageBoxResult.Yes;


            if (approval == MessageBoxResult.Yes)
            {
                foreach (var f in edgesToRemove)
                {
                    _airfield.NavigationPaths.Remove(f);
                }
                _airfield.Runways.Remove(rwy);
                RunwayList.Items.Remove(rwy);
            }
            _airfield.Runways.Remove(rwy);
            RunwayList.Items.Remove(rwy);

            _parent.DisplayGraph();

        }

        private void JunctionSave_Click(object sender, RoutedEventArgs e)
        {
            var jct = ((Airfield.Junction)(JunctionList.SelectedItem));

            if (jct == null) return;

            var oldName = jct.Name;
            jct.Name = JunctionNameBox.Text;
            try
            {
                jct.Latitude = Double.Parse(JunctionLatBox.Text);
                jct.Longitude = Double.Parse(JunctionLongBox.Text);
            }
            catch
            {
                MessageBox.Show("Failed to interpret one of your inputs to a valid number. Data not saved.");
            }
            
            foreach(var f in _airfield.NavigationPaths)
            {
                if (f.Source == oldName) f.Source = jct.Name;
                if (f.Target == oldName) f.Target = jct.Name;
            }

            _parent.DisplayGraph();
        }

        private void JunctionDelete_Click(object sender, RoutedEventArgs e)
        {
            var jct = ((Airfield.Junction)(JunctionList.SelectedItem));

            if (jct == null) return;
            var edgesToRemove = new List<Airfield.NavigationPath>();

            foreach (var f in _airfield.NavigationPaths)
            {
                if (f.Source == jct.Name) edgesToRemove.Add(f);
                else if (f.Target == jct.Name) edgesToRemove.Add(f);
            }
            MessageBoxResult approval;
            if (edgesToRemove.Count != 0)
            {
                approval = MessageBox.Show($"Node {jct.Name} has {edgesToRemove.Count} connections. Continuing will remove them as well. Continue to remove node?", "Node has connections", MessageBoxButton.YesNo, MessageBoxImage.Question);
            }
            else approval = MessageBoxResult.Yes;


            if (approval == MessageBoxResult.Yes)
            {
                foreach (var f in edgesToRemove)
                {
                    _airfield.NavigationPaths.Remove(f);
                }
                _airfield.Junctions.Remove(jct);
                JunctionList.Items.Remove(jct);
            }

            _airfield.Junctions.Remove(jct);
            JunctionList.Items.Remove(jct);


            _parent.DisplayGraph();
        }

        private void ParkingSpotBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count == 0) return;

            var jct = ((Airfield.ParkingSpot)(e.AddedItems[0]));


            ParkingSpotName.Text = jct.Name;
            ParkingSpotLat.Text = jct.Latitude.ToString();
            ParkingSpotLong.Text = jct.Longitude.ToString();

            _parent.HighlightedPoint = jct;
            _parent.DisplayGraph();
        }

        private void ParkingSpotSave_Click(object sender, RoutedEventArgs e)
        {
            var ps = ((Airfield.ParkingSpot)(ParkingSpotBox.SelectedItem));
            if (ps == null) return;
            var oldName = ps.Name;
            ps.Name = ParkingSpotName.Text;
            try
            {
                ps.Latitude = Double.Parse(ParkingSpotLat.Text);
                ps.Longitude = Double.Parse(ParkingSpotLong.Text);
            }
            catch
            {
                MessageBox.Show("Failed to interpret one of your inputs to a valid number. Data not saved.");
            }
            foreach (var f in _airfield.NavigationPaths)
            {
                if (f.Source == oldName) f.Source = ps.Name;
                if (f.Target == oldName) f.Target = ps.Name;
            }
            _parent.DisplayGraph();
        }

        private void ParkingSpotDelete_Click(object sender, RoutedEventArgs e)
        {
            var jct = ((Airfield.ParkingSpot)(ParkingSpotBox.SelectedItem));
            if (jct == null) return;
            var edgesToRemove = new List<Airfield.NavigationPath>();

            foreach (var f in _airfield.NavigationPaths)
            {
                if (f.Source == jct.Name) edgesToRemove.Add(f);
                else if (f.Target == jct.Name) edgesToRemove.Add(f);
            }
            MessageBoxResult approval;
            if (edgesToRemove.Count != 0)
            {
                approval = MessageBox.Show($"Node {jct.Name} has {edgesToRemove.Count} connections. Continuing will remove them as well. Continue to remove node?", "Node has connections", MessageBoxButton.YesNo, MessageBoxImage.Question);
            }
            else approval = MessageBoxResult.Yes;

            
            if (approval == MessageBoxResult.Yes)
            {
                foreach(var f in edgesToRemove)
                {
                    _airfield.NavigationPaths.Remove(f);
                }
                _airfield.ParkingSpots.Remove(jct);
                ParkingSpotBox.Items.Remove(jct);
            }
            _parent.DisplayGraph();
        }

    }

}
