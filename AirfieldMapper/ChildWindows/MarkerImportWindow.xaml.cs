﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Newtonsoft.Json;

namespace RurouniJones.DCS.AirfieldMapper.ChildWindows
{

    public partial class MarkerImportWindow : Window
    {
        public Markers ImportData;
        public Airfield Airfield { get; set; }
        public MainWindow ParentWindow { get; set; }

        public MarkerImportWindow(Airfield airfield, MainWindow mainWindow)
        {
            InitializeComponent();
            ParentWindow = mainWindow;
            Airfield = airfield;
        }

        private void MarkerImportButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ImportData = JsonConvert.DeserializeObject<Markers>(File.ReadAllText(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "temp", "markers-edited.json")));

                foreach (var x in ImportData.SavedMarkers)
                {
                    //Skip if already in the airport...
                    if (Airfield.NavigationGraph.Vertices.FirstOrDefault(v => v.Latitude == x.Lat && v.Longitude == x.Lon) != null) continue;

                    MarkerListBox.Items.Add(x);
                    x.NavigationPoint = new Airfield.NavigationPoint() { Latitude = x.Lat, Longitude = x.Lon, Name = x.Name };
                    Airfield.NavigationGraph.AddVertex(x.NavigationPoint);
                }

                ParentWindow.DisplayGraph();
            }
            catch(Exception)
            {
                MessageBox.Show("Failed to load marker json");
            }
        }

        private void MarkerListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count != 1) return;
            ParentWindow.HighlightedPoint = ((Marker)(e.AddedItems[0])).NavigationPoint;
            ParentWindow.DisplayGraph();
        }

        private void RunwayEndButton_Click(object sender, RoutedEventArgs e)
        {
            if (MarkerListBox.SelectedIndex == -1) return;
            var sel = CategorizationPrep();

            var rwy = new Airfield.Runway() { Latitude = sel.Lat, Longitude = sel.Lon, Name = sel.Name };
            Airfield.NavigationGraph.AddVertex(rwy);
            Airfield.Runways.Add(rwy);
            ParentWindow.DisplayGraph();
        }

        private void IntersectionButton_Click(object sender, RoutedEventArgs e)
        {
            if (MarkerListBox.SelectedIndex == -1) return;
            var sel = CategorizationPrep();


            var jct = new Airfield.Junction() { Latitude = sel.Lat, Longitude = sel.Lon, Name = sel.Name };
            Airfield.NavigationGraph.AddVertex(jct);
            Airfield.Junctions.Add(jct);
            ParentWindow.DisplayGraph();
        }

        private Marker CategorizationPrep()
        {
            ParentWindow.HighlightedPoint = null;
            var sel = ((Marker)(MarkerListBox.SelectedItem));
            Airfield.NavigationGraph.RemoveVertex(sel.NavigationPoint);
            MarkerListBox.Items.Remove(sel);
            MarkerListBox.SelectedIndex = 0;

            return sel;
        }

        private void ParkingButton_Click(object sender, RoutedEventArgs e)
        {
            if (MarkerListBox.SelectedIndex == -1) return;
            var sel = CategorizationPrep();

            var pkg = new Airfield.ParkingSpot() { Latitude = sel.Lat, Longitude = sel.Lon, Name = sel.Name };
            Airfield.NavigationGraph.AddVertex(pkg);
            Airfield.ParkingSpots.Add(pkg);
            ParentWindow.DisplayGraph();
        }
    }

}
